import React, { Fragment } from 'react';
import ApolloClient, { InMemoryCache } from 'apollo-boost';
import { BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom';

import Header from './components/Header';

import Registro from './components/Auth/Registro';
import Login from './components/Auth/Login';

import Session from './components/Session';

const App = ({refetch, session}) => {
  const { obtenerUsuario } = session;

  const mensaje =  (obtenerUsuario) ? `Bienvenido: ${obtenerUsuario.usuario}` :  '<Redirect to="/login" />'

  <ApolloProvider client={client}>
    <Router>
      <Fragment>
        <Header session={session}/>
        <div className="container">
          <p className="text-right">{mensaje}</p>
            <Switch>
              <Route exact path="/registro" component={Registro} />
              <Route exact path="/login" render={() => <Login refetch={refetch} />} />
            </Switch>
        </div>
      </Fragment>
    </Router>
  </ApolloProvider>
}

const RootSession = Session(App);

export { RootSession };